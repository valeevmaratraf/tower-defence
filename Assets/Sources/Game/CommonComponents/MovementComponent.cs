using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefence
{
    public class MovementComponent : MonoBehaviour
    {
        public IMovementTarget CurrentMovementTarget { get; set; }

        public float Speed { get; set; }

        private void Update()
        {
            MoveToDestinationPositition();
        }

        private void MoveToDestinationPositition()
        {
            if (CurrentMovementTarget == null)
            {
                return;
            }

            var direction = CurrentMovementTarget.CurrentPosition - transform.position;
            var delta = direction.normalized * Speed * Time.deltaTime;

            var deltaSqr = delta.sqrMagnitude;
            if (deltaSqr > Vector3.SqrMagnitude(transform.position - CurrentMovementTarget.CurrentPosition))
            {
                transform.position = CurrentMovementTarget.CurrentPosition;
            }
            else
            {
                transform.position += delta;
            }
        }
    }
}
