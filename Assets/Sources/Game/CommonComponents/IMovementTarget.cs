﻿using UnityEngine;

namespace TowerDefence
{
    public interface IMovementTarget
    {
        Vector3 CurrentPosition { get; }
    }
}
