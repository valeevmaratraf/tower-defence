using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.WSA;
using Zenject;
using static UnityEngine.Experimental.Rendering.RayTracingAccelerationStructure;

namespace TowerDefence
{
    [RequireComponent(typeof(MovementComponent))]
    public class Dot : MonoBehaviour
    {
        private MovementComponent movementComponent;
        private Transform launchedDotsParent;

        public IMovementTarget CurrentTarget
        {
            get => movementComponent.CurrentMovementTarget;
            set => movementComponent.CurrentMovementTarget = value;
        }

        public event Action<Dot> OnLaunch;
        public bool Launched { get; private set; }

        [Inject]
        private void Construct(ConstructArgs args)
        {
            movementComponent = GetComponent<MovementComponent>();
            movementComponent.Speed = args.Settings.DotSpeed;
            launchedDotsParent = args.LaunchedDotsParent;
        }

        public void TryLaunch(IMovementTarget target)
        {
            if (Launched)
            {
                return;
            }

            Launched = true;
            CurrentTarget = target;
            transform.SetParent(launchedDotsParent);
            OnLaunch?.Invoke(this);
        }

        public class ConstructArgs
        {
            public Transform LaunchedDotsParent;
            public GameSettings Settings;
        }
    }
}
