using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TowerDefence
{
    public class DotsCreator
    {
        private GameObject baseDotPrefab;

        public DotsCreator(GameObject baseDotPrefab)
        {
            this.baseDotPrefab = baseDotPrefab;
        }

        public Dot CreateDot(Vector3 at)
        {
            return Object.Instantiate(baseDotPrefab, at, Quaternion.identity).
                GetComponent<Dot>();
        }
    }
}
