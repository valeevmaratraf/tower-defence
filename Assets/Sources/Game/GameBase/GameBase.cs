using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static UnityEngine.Experimental.Rendering.RayTracingAccelerationStructure;

namespace TowerDefence
{
    public class GameBase : MonoBehaviour, IMovementTarget
    {
        private GameSettings settings;
        private DotsCreator dotsCreator;

        private List<Dot> dots;
        private List<DotPosition> dotsPositions;
        private float lastDotCreationTimeElapsed;
        private float lastDotLaunchTimeElapsed;

        [SerializeField] private Transform core;
        [SerializeField] private Transform dotsPositionsRoot;

        public Vector3 CurrentPosition => transform.position;

        public event Action<Dot> DotLaunched;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            dots = new List<Dot>();

            dotsCreator = args.DotsCreator;
            settings = args.Settings;

            dotsPositions = new List<DotPosition>(GameSettings.MAX_DOTS);
            dotsPositionsRoot.GetComponentsInChildren(dotsPositions);

            args.Detector.EnemyDetected += EnemyDetectedHandler;
        }

        private void Start()
        {
            for (int i = 0; i < GameSettings.MAX_DOTS; i++)
            {
                CreateNewDot();
            }
        }

        private void Update()
        {
            lastDotCreationTimeElapsed += Time.deltaTime;
            lastDotLaunchTimeElapsed += Time.deltaTime;

            if (dots.Count < GameSettings.MAX_DOTS && lastDotCreationTimeElapsed > settings.DotRespawnDelay)
            {
                CreateNewDot();
            }

            Rotate();
        }

        private void CreateNewDot()
        {
            var newDot = dotsCreator.CreateDot(transform.position);
            newDot.OnLaunch += RemoveDot;
            var freePosition = GetDotPosition(false);
            freePosition.LinkDotToPosition(newDot);
            dots.Add(newDot);
            lastDotCreationTimeElapsed = 0;
        }

        private DotPosition GetDotPosition(bool isBusy)
        {
            foreach (var dotPosition in dotsPositions)
            {
                if (dotPosition.IsBusy == isBusy)
                {
                    return dotPosition;
                }
            }

            return null;
        }

        private void EnemyDetectedHandler(IEnemy enemy)
        {
            var busyDotPosition = GetDotPosition(true);
            if (busyDotPosition == null)
            {
                return;
            }

            if (lastDotLaunchTimeElapsed >= settings.BaseDotsLaunchDelay)
            {
                busyDotPosition.CurrentDot.TryLaunch(enemy);
                lastDotLaunchTimeElapsed = 0;
            }
        }

        private void RemoveDot(Dot dot)
        {
            dots.Remove(dot);
            DotLaunched?.Invoke(dot);
        }

        //TODO: extract to another class
        private void Rotate()
        {
            float fullCycleByDegree = Mathf.PI * 2 * Mathf.Rad2Deg;
            dotsPositionsRoot.Rotate(Vector3.forward, settings.BaseRotationSpeed * Time.deltaTime * fullCycleByDegree);
        }

        public class ConstructArgs
        {
            public GameSettings Settings;
            public DotsCreator DotsCreator;
            public EnemyDetector Detector;
        }
    }
}
