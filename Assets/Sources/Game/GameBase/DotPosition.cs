using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefence
{
    public class DotPosition : MonoBehaviour, IMovementTarget
    {
        public bool IsBusy => CurrentDot != null;

        public Dot CurrentDot { get; private set; }

        public Vector3 CurrentPosition => transform.position;

        public void LinkDotToPosition(Dot dot)
        {
            if (CurrentDot != null)
            {
                Debug.LogWarning("Cant submit dot coz its already busy!", this);
                return;
            }

            CurrentDot = dot;
            dot.CurrentTarget = this;
            dot.transform.SetParent(transform);
            dot.OnLaunch += DotLaunchHandler;
        }

        private void DotLaunchHandler(Dot dot)
        {
            if (CurrentDot != dot)
            {
                Debug.LogError("Unexpected behaviour!", this);
                return;
            }

            CurrentDot.OnLaunch -= DotLaunchHandler;
            CurrentDot = null;
        }
    }
}
