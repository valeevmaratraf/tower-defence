using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TowerDefence
{
    public class EnemyDetector : MonoBehaviour
    {
        private EnemyProvider enemyProvider;
        private GameSettings settings;

        public event Action<IEnemy> EnemyDetected;

        [Inject]
        private void Construct(EnemyProvider enemyProvider, GameSettings settings)
        {
            this.settings = settings;
            this.enemyProvider = enemyProvider;
        }

        private void Update()
        {
            float detectionRadiusSqr = settings.DetectionRadius * settings.DetectionRadius;

            foreach (var enemy in enemyProvider.Enemies)
            {
                if (Vector3.SqrMagnitude(enemy.CurrentPosition - transform.position) < detectionRadiusSqr)
                {
                    EnemyDetected?.Invoke(enemy);
                }
            }
        }
    }
}
