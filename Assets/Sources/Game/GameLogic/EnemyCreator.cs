using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static UnityEngine.Experimental.Rendering.RayTracingAccelerationStructure;

namespace TowerDefence
{
    public class EnemyCreator : MonoBehaviour
    {
        public event Action<IEnemy> EnemyCreated;

        private GameSettings settings;
        private GameObject enemyPrefab;
        private Transform spawnAreaCenter;

        private float lastEnemySpawnElapsedTime;

        private int enemyCounter;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            settings = args.Settings;
            enemyPrefab = args.EnemyPrefab;
            spawnAreaCenter = args.SpawnAreaCenter;
        }

        private void Update()
        {
            if (lastEnemySpawnElapsedTime >= settings.EnemyRespawnDelay)
            {
                enemyCounter++;
                SpawnEnemy();
                lastEnemySpawnElapsedTime = 0;
            }

            lastEnemySpawnElapsedTime += Time.deltaTime;
        }

        private void SpawnEnemy()
        {
            var enemyGO = Instantiate(enemyPrefab, spawnAreaCenter);
            enemyGO.name = $"Enemy {enemyCounter}";
            var enemyT = enemyGO.transform;

            float randomDegree = UnityEngine.Random.Range(0, 360);
            enemyT.position = spawnAreaCenter.position;
            enemyT.rotation *= Quaternion.AngleAxis(randomDegree, Vector3.back);
            enemyT.transform.position += enemyT.right * settings.EnemySpawnRadius;
            enemyT.Rotate(Vector3.back, 180);

            EnemyCreated?.Invoke(enemyGO.GetComponent<IEnemy>());
        }

        public class ConstructArgs
        {
            public GameSettings Settings;
            public GameObject EnemyPrefab;
            public Transform SpawnAreaCenter;
        }
    }
}
