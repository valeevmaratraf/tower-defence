using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TowerDefence
{
    public class EnemyProvider
    {
        private List<IEnemy> enemies;

        private EnemyCreator enemyCreator;

        public IReadOnlyList<IEnemy> Enemies => enemies.AsReadOnly();

        [Inject]
        private void Construct(EnemyCreator enemyCreator)
        {
            this.enemyCreator = enemyCreator;
            enemies = new List<IEnemy>();

            this.enemyCreator.EnemyCreated += RegisterEnemy;
        }

        private void RegisterEnemy(IEnemy enemy)
        {
            enemies.Add(enemy);
            enemy.Destroyed += UnregisterEnemy;
        }

        private void UnregisterEnemy(IEnemy enemy)
        {
            enemies.Remove(enemy);
            enemy.Destroyed -= UnregisterEnemy;
        }
    }
}
