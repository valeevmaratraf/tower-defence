using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TowerDefence
{
    public class LaunchedDotProvider
    {
        private GameBase gameBase;

        private List<Dot> dots;

        public IReadOnlyList<Dot> Dots => dots.AsReadOnly();

        [Inject]
        private void Construct(GameBase gameBase)
        {
            dots = new List<Dot>();
            this.gameBase = gameBase;
            this.gameBase.DotLaunched += DotLaunchHandler;
        }

        private void DotLaunchHandler(Dot dot)
        {
            dots.Add(dot);
        }
    }
}
