using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TowerDefence
{
    public class SceneInstaller : MonoInstaller
    {
        [SerializeField] private GameSettings gameSettings;

        [SerializeField] private GameBase gameBase;
        [SerializeField] private GameObject dotPrefab;

        [SerializeField] private GameObject enemyPrefab;
        [SerializeField] private EnemyDetector enemyDetector;
        [SerializeField] private EnemyCreator enemyCreator;
        [SerializeField] private Transform enemySpawnAreaCenter;
        [SerializeField] private Transform launchedDotsParent;

        public override void InstallBindings()
        {
            var dotsCreator = new DotsCreator(dotPrefab);
            
            Container.Bind<GameSettings>().FromInstance(gameSettings).AsSingle();

            EnemyCreator.ConstructArgs enemyCreatorArgs = new EnemyCreator.ConstructArgs()
            {
                EnemyPrefab = enemyPrefab,
                Settings = gameSettings,
                SpawnAreaCenter = enemySpawnAreaCenter
            };
            Container.Bind<EnemyCreator.ConstructArgs>().FromInstance(enemyCreatorArgs).AsSingle();

            Enemy.ConstructArgs enemyArgs = new Enemy.ConstructArgs()
            {
                Target = gameBase,
                Settings = gameSettings
            };
            Container.Bind<Enemy.ConstructArgs>().FromInstance(enemyArgs).AsSingle();

            GameBase.ConstructArgs baseArgs = new GameBase.ConstructArgs()
            {
                Detector = enemyDetector,
                DotsCreator = dotsCreator,
                Settings = gameSettings
            };
            Container.Bind<GameBase.ConstructArgs>().FromInstance(baseArgs).AsSingle();

            Dot.ConstructArgs dotArgs = new Dot.ConstructArgs()
            {
                Settings = gameSettings,
                LaunchedDotsParent = launchedDotsParent
            };
            Container.Bind<Dot.ConstructArgs>().FromInstance(dotArgs).AsSingle();

            Container.Bind<EnemyCreator>().FromInstance(enemyCreator).AsSingle();
            Container.Bind<EnemyProvider>().FromNew().AsSingle();
            Container.Bind<EnemyDetector>().FromInstance(enemyDetector).AsSingle();

            Container.Bind<GameBase>().FromInstance(gameBase).AsSingle();
        }
    }
}
