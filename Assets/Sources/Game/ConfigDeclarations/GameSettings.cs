using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefence
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "TowerDefence/Settings/GameSettings", order = 0)]

    public class GameSettings : ScriptableObject
    {
        public const int MAX_DOTS = 8;

        [field: SerializeField] public float BaseRotationSpeed { get; set; } = 0.25F;
        [field: SerializeField] public float DotSpeed { get; set; } = 3;
        [field: SerializeField] public float DotRespawnDelay { get; set; } = 1;
        [field: SerializeField] public float EnemyRespawnDelay { get; set; } = 1;
        [field: SerializeField] public float EnemySpawnRadius { get; set; } = 25;
        [field: SerializeField] public float EnemySpeed { get; set; } = 2;
        [field: SerializeField] public float DetectionRadius { get; set; } = 20;
        [field: SerializeField] public float BaseDotsLaunchDelay { get; set; } = 1;
        [field: SerializeField] public int BaseHP { get; set; } = 10;


        private void OnValidate()
        {
            if (DotSpeed < 0.1F)
            {
                DotSpeed = 0.1F;
            }

            if (EnemySpeed < 0.1F)
            {
                EnemySpeed = 0.1F;
            }

            if (DotRespawnDelay < 0.1F)
            {
                DotRespawnDelay = 0.1F;
            }

            if (EnemySpawnRadius < 0)
            {
                EnemySpawnRadius = 0;
            }

            if (DetectionRadius < 0)
            {
                DetectionRadius = 0;
            }

            if (BaseHP < 1)
            {
                BaseHP = 1;
            }
        }
    }
}
