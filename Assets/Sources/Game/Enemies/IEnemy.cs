using System;
using System.Collections;
using System.Collections.Generic;

namespace TowerDefence
{
    public interface IEnemy : IMovementTarget
    {
        event Action<IEnemy> Destroyed;
    }
}
