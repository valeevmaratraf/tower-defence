using System;
using System.Collections;
using System.Collections.Generic;
using TowerDefence;
using UnityEngine;
using Zenject;

namespace TowerDefence
{
    [RequireComponent(typeof(MovementComponent))]
    public class Enemy : MonoBehaviour, IMovementTarget, IEnemy
    {
        public Vector3 CurrentPosition => transform.position;

        public event Action<IEnemy> Destroyed;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            var movementComponent = GetComponent<MovementComponent>();
            movementComponent.Speed = args.Settings.EnemySpeed;
            movementComponent.CurrentMovementTarget = args.Target;
        }

        private void OnDestroy()
        {
            Destroyed?.Invoke(this);
        }

        public class ConstructArgs
        {
            public IMovementTarget Target;
            public GameSettings Settings;
        }
    }
}
